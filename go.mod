module gitlab.com/armanvp-lib/tempo

go 1.19

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	gitlab.com/armanvp-lib/datatypes v0.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
