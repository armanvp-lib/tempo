package tempo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/armanvp-lib/datatypes/date"
)

const Endpoint = "https://api.tempo.io/4"

type Tempo struct {
	config Config
	client *http.Client
}

func New(c Config) *Tempo {
	return &Tempo{
		config: c,
		client: &http.Client{},
	}
}

func (t *Tempo) GetWorklogs(from, to date.Date) (worklogs []*Worklog, err error) {
	url := fmt.Sprintf("%s/worklogs/user/%s?from=%s&to=%s", Endpoint, t.config.User, from, to)

	wr := &WorklogResponse{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("error initialising request: %w", err)
	}
	req.Header.Set("Authorization", t.config.GetBearerAuth())

	resp, err := t.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error fetching URL: %w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("error closing body: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	bs, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response: %w", err)
	}

	err = json.Unmarshal(bs, &wr)
	if err != nil {
		return nil, fmt.Errorf("error parsing response: %w", err)
	}

	for _, result := range wr.Results {
		worklogs = append(worklogs, &result)
	}

	return
}

func (t *Tempo) CreateWorklog(w *WorklogRequest) (worklog *Worklog, err error) {
	url := fmt.Sprintf("%s/worklogs", Endpoint)

	if w.User == "" {
		w.User = t.config.User
	}

	bs, err := json.Marshal(*w)
	if err != nil {
		return nil, fmt.Errorf("error marshalling request: %w", err)
	}

	br := bytes.NewReader(bs)

	req, err := http.NewRequest("POST", url, br)
	if err != nil {
		return nil, fmt.Errorf("error initialising request: %w", err)
	}
	req.Header.Set("Authorization", t.config.GetBearerAuth())
	req.Header.Set("Content-Type", "application/json")

	resp, err := t.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error posting to URL: %w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("error closing body: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	bsresp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response: %w", err)
	}

	worklog = &Worklog{}

	err = json.Unmarshal(bsresp, worklog)
	if err != nil {
		return nil, fmt.Errorf("error parsing response: %w", err)
	}

	return
}

func (t *Tempo) DeleteWorklog(id int) (err error) {
	url := fmt.Sprintf("%s/worklogs/%d", Endpoint, id)

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return fmt.Errorf("error initialising request: %w", err)
	}
	req.Header.Set("Authorization", t.config.GetBearerAuth())

	resp, err := t.client.Do(req)
	if err != nil {
		return fmt.Errorf("error posting to URL: %w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("error closing body: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	return
}

func (t *Tempo) GetAccount(id int) (a *Account, err error) {
	url := fmt.Sprintf("%s/accounts/%d", Endpoint, id)

	a = &Account{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("error initialising request: %w", err)
	}
	req.Header.Set("Authorization", t.config.GetBearerAuth())

	resp, err := t.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error fetching URL: %w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("error closing body: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	bs, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response: %w", err)
	}

	err = json.Unmarshal(bs, &a)
	if err != nil {
		return nil, fmt.Errorf("error parsing response: %w", err)
	}

	return
}
