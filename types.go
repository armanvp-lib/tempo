package tempo

type Account struct {
	Key    string `json:"key"`
	Id     int    `json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
	Global bool   `json:"global"`
}

type WorklogAttributeValue struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type WorklogAttribute struct {
	Values []WorklogAttributeValue `json:"values"`
}

type WorklogIssue struct {
	Id int `json:"id"`
}

type Worklog struct {
	ID               int              `json:"tempoWorklogId,omitempty"`
	TimeSpentSeconds int              `json:"timeSpentSeconds"`
	StartDate        string           `json:"startDate"`
	Description      string           `json:"description"`
	Attributes       WorklogAttribute `json:"attributes"`
	Issue            WorklogIssue     `json:"issue,omitempty"`
}

type WorklogResponse struct {
	Results []Worklog `json:"results"`
}

type WorklogRequest struct {
	TimeSpentSeconds int                     `json:"timeSpentSeconds"`
	StartDate        string                  `json:"startDate"`
	Description      string                  `json:"description"`
	Attributes       []WorklogAttributeValue `json:"attributes"`
	User             string                  `json:"authorAccountId"`
	IssueID          int                     `json:"issueId"`
}
