package tempo

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestConfig_GetBearerAuth(t *testing.T) {
	r := require.New(t)

	c := Config{
		User:  "dummy@dummy.com",
		Token: "abcdef123456",
	}

	// base64 encoded value of the "<email>:<token>"
	expected := "Bearer abcdef123456"

	r.Equal(expected, c.GetBearerAuth(), "Should have the expected bearer auth value")
}
