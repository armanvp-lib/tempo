package tempo

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/armanvp-lib/datatypes/date"
)

func TestTempo_GetWorklogs(t *testing.T) {
	r := require.New(t)
	c := Config{
		User:  os.Getenv("USER"),
		Token: os.Getenv("TOKEN"),
	}

	tempo := New(c)
	dt, err := date.New("2022-11-02")
	r.NoError(err, "New date should return no error")

	wls, err := tempo.GetWorklogs(dt, dt)
	r.NoError(err, "GetWorklogs should return no error")
	r.NotNil(wls, "GetWorklogs should return not nil")
}

func TestTempo_CreateWorklog(t *testing.T) {
	r := require.New(t)
	c := Config{
		User:  os.Getenv("USER"),
		Token: os.Getenv("TOKEN"),
	}

	tempo := New(c)
	wl, err := tempo.CreateWorklog(&WorklogRequest{
		TimeSpentSeconds: 60,
		StartDate:        time.Now().Format("2006-01-02"),
		Description:      "Stand",
		Attributes: []WorklogAttributeValue{
			{
				Key:   "_WorkType_",
				Value: "TeamCeremonies",
			},
			{
				Key:   "_CER_",
				Value: "NOCER",
			},
		},
		IssueID: 11141,
	})
	r.NoError(err, "CreateWorklog should return no error")
	r.NotNil(wl, "CreateWorklog should return not nil")

	err = tempo.DeleteWorklog(wl.ID)
	r.NoError(err, "DeleteWorklog should return no error")
}
