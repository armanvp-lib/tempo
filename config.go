package tempo

import (
	"fmt"
)

type Config struct {
	User  string
	Token string
}

func (c Config) GetBearerAuth() string {
	auth := fmt.Sprintf("Bearer %s", c.Token)

	return auth
}
